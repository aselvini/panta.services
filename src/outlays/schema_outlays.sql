BEGIN TRANSACTION;

CREATE TABLE "outlays" (
	"price"	real NOT NULL,
	"currency"	text NOT NULL,
  "rate" text,
	"timestamp"	INTEGER NOT NULL,
	"notes"	text,
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE
);

COMMIT;