import { list } from './list.js';
import { add, remove } from './entry.js';

import config from '../config.js';
import SQLite from 'better-sqlite3';

const version = '1.0.0';

const Outlays = (app) => {
  const db = initDb();

  return {
    version,
    list: list(db),
    add: add(db),
    remove: remove(db)
  };
}

const initDb = () => {

  const storeFile = config.outlays.store.path;
  console.debug('Loading store file ', storeFile);

  return new SQLite(storeFile, {
    verbose: console.log,
    fileMustExist: true
  });
};

export default Outlays;