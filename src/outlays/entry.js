import {CREATE, REMOVE} from './queries';

const add = db => ctx => {
  const {price, currency, timestamp, notes} = ctx.request.body;
  const stmt = db.prepare(CREATE);

  if ([price, currency, timestamp].some( v => v === undefined)) {
    ctx.status = 400;
    return;
  }

  try {
    const args = {
      price,
      currency,
      timestamp,
      notes: JSON.stringify(notes)
    };

    stmt.run(args);
    ctx.status = 204;

  } catch(error) {
    ctx.status = 500;
    ctx.body = error.message
    throw error;
  }
}

const remove = db => ctx => {
  const {id} = ctx.request.body;
  const stmt = db.prepare(REMOVE);

  if (id === undefined) {
    ctx.status = 400;
    return;
  }

  try {
    stmt.run(id);
    ctx.status = 204;

  } catch(error) {
    ctx.status = 500;
    ctx.body = error.message
    throw error;
  }
}

export {add, remove};