import { selectByCategory, selectByTime } from './queries';

const getTodayMSatHMS = (hours = 0, minutes = 0, seconds = 0, ms = 0) => {
  const today = new Date();
  today.setHours(hours, minutes, seconds, ms);
  return today.getTime();
};

export const list = db => ctx => {
  const {from:fromStr, to:toStr, categories} = ctx.query;
  const select = categories ? selectByCategory(categories.length) : selectByTime;

  const list = db.prepare(select);

  const fromMs = fromStr ? parseInt(fromStr, 10) :  getTodayMSatHMS();
  const toMs = toStr ? parseInt(toStr, 10) :  getTodayMSatHMS(23,59,59);

  try {
    const args = [ fromMs, toMs ].concat( categories ? categories.split(',').map(n=>parseInt(n,10)) : []);
    const values = list.all(args);

    ctx.body = values.reduce((acc, outlay) => { 
      const {categories, ...rest} = outlay;
      return acc.concat({categories: JSON.parse(categories), ...rest});
    }, []);

  } catch(error) {
    ctx.status = 500;
    ctx.body = error.message
    throw error;
  }
}