import request from 'supertest';
import koa from 'koa';
import SQLite from 'better-sqlite3';
import fs from 'fs';
import process from 'process';

import { register } from './index.js';
import config from '../../config.js';

const todayStart = new Date();
todayStart.setHours(0,0,0,0);

const todayEnd = new Date();
todayEnd.setHours(23,59,59,0);

const d20050125 = 1106607600000;

let server;
let db;
const hour = 60*60*1000;
const DATA = [
  [10,'EUR', 980377200000, '{"categories":[1]}'],
  [20,'EUR', 1011913200000, '{"categories":[2]}'],
  [30,'EUR', d20050125, '{"categories":[1,3]}'],
  [40,'EUR', 1450998000000, '{"categories":[4,5]}'],
  [50,'EUR', 1545692400000, '{"categories":[5,6]}'],
  [60,'EUR', todayStart.getTime()+hour, '{"categories":[7,8]}'],
  [70,'EUR', todayEnd.getTime()-hour, '{"categories":[8,9]}'],
  [80,'EUR', todayEnd.getTime()+(hour*24), '{"categories":[10]}']
];

function loadSqlSchema() {
  const cwd = process.cwd();
  const sqlFile = cwd + '/src/outlays/schema_outlays.sql';

  if (!fs.existsSync(sqlFile)){
    throw new Error('SQL file not found in ' + cwd);
  }
  return fs.readFileSync(sqlFile).toString();
}

function mockData() {
  if (!db) {
    throw new Error('db missing');
  }
  const stmt = db.prepare('INSERT INTO outlays (price, currency, timestamp, notes) VALUES(?,?,?,?)');

  const insertOutlays = db.transaction((outlays) => {
    outlays.forEach(outlay => stmt.run(outlay));
  });
  
  insertOutlays(DATA);
}

const schema = loadSqlSchema();
let agentWithCookies;

beforeEach(() => {

  db = new SQLite('outlaysTestDb', {
      memory: true,
  //    verbose: console.log,
      fileMustExist: false
    });

  db.exec(schema);

  mockData();

  const app = new koa();
  app.context.session = {logged: true};
  register(app, db);
  server = app.listen(8234)

  agentWithCookies = request.agent(server)
        .set('Cookie',[config.authCookieName+'=auth'])

});

afterEach(() => {
  db.close();
  server && server.close();
});

describe('Outlays test', () => {

  describe('/outlays/list', () => {
    it('should show today\'s list without params', async () => {
      const list = await agentWithCookies
        .get('/outlays/list')
        .query();

      expect(list.body.length).toEqual(2);
    });

    it('with from only list all entries from date to today\'s midnight', async () => {
      const list = await agentWithCookies
        .get(`/outlays/list?from=${d20050125}`)
        .query();

      expect(list.body.length).toEqual(5);
    });

    it('with from and to', async () => {
      const list = await agentWithCookies
        .get(`/outlays/list?from=${d20050125}&to=${todayStart.getTime()}`)
        .query();

      expect(list.body.length).toEqual(3);
    });

    it('with categories', async () => {
      const list = await agentWithCookies
        .get('/outlays/list?from=0&categories=2,1')
        .query();

      expect(list.body.length).toEqual(3);
    })
  })

  describe('/outlays/add', () => {
    it('create new entry', async () => {
      const connection = agentWithCookies;
      const ms = (new Date).getTime();
      const price = 99.99;
      const currency = 'EUR';
      const category = 2;
  
      const insert = await connection
        .post('/outlays/add')
        .send({
          price,
          currency,
          timestamp: ms,
          notes: {
            categories: [1,category,3]
          }
        })
        .expect(204)
        .then(async () => {
            const list = await connection
              .get(`/outlays/list?from=${ms}&to=${ms}`)
              .query()
      
            expect(list.body.length).toEqual(1)

            const entry = list.body[0];

            expect(entry.price).toEqual(price);
            expect(entry.currency).toEqual(currency);
            expect(entry.categories[1]).toEqual(category);
            expect(entry.timestamp).toEqual(ms);
        });

    });


    describe('check parameters', () => {
      const timestamp = (new Date).getTime();
      const price = 100;
      const currency = 'EUR';
  
      it('currency must be defined', async () => {
        await agentWithCookies
          .post('/outlays/add')
          .send({
            price,
            currency: undefined,
            timestamp
          })
          .expect(400);
      });

      it('timestamp must be defined', async () => {
        await agentWithCookies
          .post('/outlays/add')
          .send({
            price,
            currency,
            timestamp: undefined
          })
          .expect(400);
      });

      it('price must be defined', async () => {
        await agentWithCookies
          .post('/outlays/add')
          .send({
            price: undefined,
            currency,
            timestamp
          })
          .expect(400);
      });
    });
  });

  describe('/outlays/remove', () => {
    it('should remove entry by id', async () => {
        const ms = 1234;
        await agentWithCookies
          .post('/outlays/add')
          .send({price: 10, timestamp: ms, currency: 'EUR'})
          .expect(204)
          .then( async () => {

            await agentWithCookies
              .get(`/outlays/list?from=${ms}&to=${ms}`)
              .query()
              .then(async (response) => {

                expect(response.body.length).toEqual(1);

                const id = response.body[0].id;
                
                expect(id).toBeDefined();

                await agentWithCookies
                  .delete('/outlays/remove')
                  .send({id})
                  .expect(204);

              })
          });
    });

    it('should reject calls without id', async () => {
      await agentWithCookies
        .delete('/outlays/remove')
        .send({noid:null})
        .expect(400);
    });
  });
});