const columns = `outlays.ROWID, price, currency, rate, timestamp, json_extract(outlays.notes,'$.categories') as categories`;

const selectByTime = `
  SELECT ${columns}
    FROM outlays 
   WHERE timestamp >= ? AND timestamp <= ?
ORDER BY timestamp
`;

const selectByCategory = (length) => { 
  const placeholders = (new Array(length-1)).fill('?');
  return `
  SELECT ${columns}
    FROM outlays,json_tree(outlays.notes,'$.categories')  
   WHERE timestamp >= ? AND timestamp <= ?
     AND json_tree.value 
      IN (${placeholders.join(',')})
ORDER BY timestamp
`;
};

const CREATE = `
  INSERT 
    INTO outlays (price, currency, timestamp, notes)
  VALUES (@price, @currency, @timestamp, json(@notes))
`;

const REMOVE = `
  DELETE 
    FROM outlays
   WHERE outlays.id=?
`;

export {
  selectByTime,
  selectByCategory,
  CREATE,
  REMOVE 
};