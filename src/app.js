import koa from 'koa';
import session from 'koa-session';
import sessionStore  from 'koa-sqlite3-session';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';

import config from './config.js';
import Auth from './auth/index.js';
import Outlays from './outlays/index.js';

const app = new koa();

const router = new Router();
const storeRef = new sessionStore('sessionStore.db');

app.use(cors());
app.keys = config.appKeys;

app.use(session({
  key: config.authCookieName,
  httpOnly: true,
  maxAge: 864000,
  store: storeRef,
  signed: true,
  secure: false,
  domain: '.panta.lan',
  path: '/'
}, app));

app.context.sessionStore = storeRef;

const auth = Auth(app);

router.post("/auth/login", auth.login);
router.get("/auth/logout", auth.logout);
router.get("/auth/status", auth.status);

const outlays = Outlays(app);

router.get('/outlays/list', outlays.list);
router.post('/outlays/add', outlays.add);
router.delete('/outlays/remove', outlays.remove);

app.use(bodyParser());

app.use(router.routes());
app.use(router.allowedMethods());

app.on('error', err => {
  console.error('Server error', err)
});


console.log('Listening to 2501');
const server = app.listen(2501);

export default server;