import fs from 'fs';

import bcrypt from 'twin-bcrypt';

function parser(file) {

  if (!fs.existsSync(file)) {
    return;
  }

  let contents; 
  try {
    contents = fs.readFileSync(file,'utf8');
  } catch(err) {
    console.log(`Cannot read ${file}`);
    return;
  }

  const couples = (contents.split('\n')).reduce( (acc, line) => {
    const slices = line.split(':');
    acc[slices[0]] = slices[1];
    return acc;
  }, {});

  return getUserFunc(couples);
}

function getUserFunc(couples) {
  return (username, password) => {
    let isValid = false;
    try {
      isValid = couples.hasOwnProperty(username) && bcrypt.compareSync(password, couples[ username ]);
    } catch(e) {
      if (e.message === 'Incorrect arguments') {
        console.error('Did you forget to encrypt the password of ' + username + ' with bcrypt?');
      } else {
        console.error(e);
      }
    }
    return isValid;
  }
}

export { parser };