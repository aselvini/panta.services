import config from '../../config.js';

async function checkAuthorization(ctx, next) {
  if (!ctx.cookies.get(config.authCookieName)) {
    ctx.throw(401,{code: 401, message: 'Unauthorized'});
  }
  next();
}

export { checkAuthorization };