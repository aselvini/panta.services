import koa from 'koa';
import request from 'supertest';
import session from 'koa-session';
import sessionStore  from 'koa-sqlite3-session';

import registerAuth from './index.js';
import config from '../../config.js';

let server ;

afterEach((done) => {
  server.close(done);
});

beforeEach(() => {

  const storeRef = new sessionStore('testSessionStore.db');
  const app = new koa();

  const testSession = session({
    key: config.authCookieName,
    httpOnly: true,
    maxAge: 864000,
    signed: false,
    secure: false,
    store: storeRef,
    domain: '.panta.lan',
    path: '/'
  }, app);

  app.use(testSession);
  registerAuth(app);
  server = app.listen(8235)
});



describe('Auth test', () => {
  describe('/auth/login', () => {
    it('should login with right username/password', async () => {
      const response = await request.agent(server).post('/auth/login').send({
        username: 'test',
        password: 'test'
      });

      expect(response.status).toEqual(200);
      expect(response.body.logged).toEqual(true);
    });

    it('should not login without username/password', async () => {
      const response = await request.agent(server).post('/auth/login').send({
        username: 'test',
        password: 'noway'
      });

      expect(response.status).toEqual(200);
      expect(response.body.logged).toEqual(false);
    });
  })

  describe('/auth/status', () => {
    xit('should get login status on logged', async () => {
      const connection = request.agent(server);

      await connection
        .post('/auth/login')
        .send({
          username: 'test',
          password: 'test'
        })
        .expect(200)
        .expect(res =>  expect(res.body.logged).toEqual(true) );

      const response =   await connection
          .get('/auth/status')
          .query()
          .expect(200)
      /* TODO
       * The second call doesn't keep the value in the context
       * Tested even with supertest-session
       */
      expect(response.body.logged).toEqual(true);
    });

    it('should get login status on not logged', async () => {
      const connection = request.agent(server);

      /* TODO
       * this test is not really working */
      await connection
        .post('/auth/login')
        .send({
          username: 'test',
          password: 'noway'
        })
        .expect(200)
        .expect({logged: false})
        .then( async () => {
          const status = await  connection
            .get('/auth/status')
            .query();

          expect(status.body.logged).toEqual(false);
        });
    });
  });

  describe('/auth/logout', () => {
    it('should get login status', async () => {
      const connection = request.agent(server);

      await connection
        .post('/auth/login')
        .send({
          username: 'test',
          password: 'test'
        })
        .expect(200)
        .expect(res => expect(res.body.logged).toEqual(true))
        .then( async () => {
          const status = await  connection
            .get('/auth/logout')
            .query();

          expect(status.body).toEqual({});
        });

    });
  });
});