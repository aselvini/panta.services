import config from '../config.js';
import { parser } from '../lib/htpasswd.js';

const version = '1.0.0';

const login = (ctx) => {
  const { request: {body: {username, password }}} = ctx;

  if (username === 'test' && process.env.NODE_ENV !== 'test') {
    return;
  }

  let status = {};

  if (!username || !password) {
    ctx.throw(400, "Missing username or password");
  }

  const isUser = parser(__dirname + '/../../.htpasswd');

  if (ctx.sessionStore ) { 
    ctx.sessionStore.flush();
  }

  if (isUser(username, password) === true) {
    ctx.session.logged = true;
    status = { logged: true, session: ctx.session };
  } else {
    ctx.session.logged = false;
    status = { logged: false };
  }

  ctx.body = status;
};

const status = (ctx) => {
  const key = ctx.cookies.get(config.authCookieName);

  ctx.body = {
    version, 
    key,
    session: ctx.session,
    logged: ctx.session.logged  === true
  };
}

const logout = (ctx) => {
  ctx.session = null;
  ctx.redirect("/status");
}


const auth = app => {
  return {
    version,
    status,
    login,
    logout
  };
}

export default auth;