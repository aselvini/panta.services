import fs from 'fs';
import config from '../config';
import { execSync } from 'child_process';
  
const storeFile = config.outlays.store.path;

if (fs.existsSync(storeFile)) {
  throw new Error('The store already exists');
} else {
  execSync(`sqlite3 ${storeFile} < ./src/outlays/schema_outlays.sql`)
  console.log('%s created', storeFile);
}

