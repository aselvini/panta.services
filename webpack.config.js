const os = require('os');
const path = require('path');

const CleanWebpackPlugin = require('clean-webpack-plugin');

const webpack = require('webpack');

const nodeExternals = require('webpack-node-externals');
const distFolder = 'dist';

module.exports = (env = { dev: true }, argv) => {
  const publicPath = '/';
  return {
    target: 'node',
    entry: {
      server: './src/app.js',
    },
    output: {
      path: path.resolve(__dirname, distFolder),
      publicPath: publicPath,
      filename: 'js/[name].[chunkhash].js'
    },
    externals: [nodeExternals()],
    node: {
      // Need this when working with express, otherwise the build fails
      __dirname: false,   // if you don't put this is, __dirname
      __filename: false,  // and __filename return blank or /
    },
    module: {
      rules: [
        /*
        {
          enforce: "pre",
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
          options: {
            failOnWarning: true,
            failOnError: false
          }
        },
        */
        {
          resolve: {
            extensions: [".js"]
          },
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        }/*, {
          test: /\.(png|svg|jpg|gif|json)$/,
          use: ['file-loader?name=images/[hash].[ext]']
        }, {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[hash].[ext]',
                publicPath: publicPath + '/fonts/',
                outputPath: 'fonts/'
              }
            }
          ]
        }, 
	   */
      ]
    },
    plugins: [
      new CleanWebpackPlugin([path.resolve(__dirname, distFolder)]),
      new webpack.ProvidePlugin({
        Map: 'core-js/es6/map', 
        WeakMap: 'core-js/es6/weak-map', 
        Promise: 'core-js/es6/promise', 
        regeneratorRuntime: 'regenerator-runtime' // to support await/async syntax
      })
    ],
    devServer: {
      port: 2501,
      publicPath: publicPath,
    }
  }
};
