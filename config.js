const cwd = process.cwd();

const config = {
  appKeys: ['832mdashj3u032nm'],
  authCookieName: 'panta:auth',
  outlays: {
    store: {
      path: cwd + '/db/outlays.sqlite'
    }
  }
}

export default config;